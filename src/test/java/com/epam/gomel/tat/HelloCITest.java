package com.epam.gomel.tat;


import org.testng.Assert;
import org.testng.annotations.Test;

public class HelloCITest {

    @Test
    public void sum() {
        Assert.assertEquals(1 + 2, 3);
    }

    @Test
    public void subtr() {
        Assert.assertEquals(3 - 1, 2);
    }

    @Test
    public void mult() {
        Assert.assertEquals(2 * 3, 6);
    }

    @Test
    public void div() {
        Assert.assertEquals(3.0 / 2.0, 1.5);
    }
}
